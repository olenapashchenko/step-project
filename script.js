const tabs = document.querySelector('.tabs-services')
const tabsBtn = document.querySelectorAll('.tabs-title-services')
const tabsItems = document.querySelectorAll('.services-item')

tabs.addEventListener('click', (e) => {
    tabsBtn.forEach(item => {
        if (e.target === item) {
            item.classList.add('active')
        } else { item.classList.remove('active') }
    })
    tabsItems.forEach(item => {
        if (item.dataset.tab === e.target.dataset.tab) {
            item.classList.add('active')
        } else { item.classList.remove('active') }
    })
})

const amazingTabs = document.querySelector('.amazing-work ul')
const amazingItems = document.querySelectorAll('.amazing-cards-wrapper a')
const amazingBtn = document.querySelectorAll('.amazing-work li')
const amazingBtnAll = document.querySelectorAll('.amazing-work li')[0]

amazingTabs.addEventListener('click', e => {
    amazingItems.forEach(el => {
        if (e.target.dataset.tab === el.dataset.tab) {
            el.classList.add('active')
        } else if (e.target === amazingBtnAll) {
            el.classList.add('active')
        } else {
            el.classList.remove('active')
        }
    })
    amazingBtn.forEach(el => {
        if (e.target === el) {
            el.classList.add('active')
        } else { el.classList.remove('active') }
    })
})
amazingBtnAll.click()

const btnLoad = document.querySelector('.btn-load')
const download = document.querySelector('.amazing-download')
const cardsLoad = document.querySelector('.amazing-cards-load')
const divBtnLoad = document.querySelector('.amazing-btn')

btnLoad.addEventListener('click', e => {
    e.preventDefault()
    divBtnLoad.remove()
    download.classList.add('amazing-download-visible')
    function setTimer(){
        download.classList.remove('amazing-download-visible')
        download.remove()
        cardsLoad.classList.add('amazing-cards-load-visible')
    }
    setTimeout(setTimer, 2000);
})

$(document).ready(function () {
    $('.people-slider').slick ({
        slidesToShow: 3,
        variableWidth: true,
        asNavFor: ".people-slider-main",
        focusOnSelect: true,
    })
    $('.people-slider-main').slick({
        arrows: false,
        fade: true,
        asNavFor: ".people-slider",
    })
})

